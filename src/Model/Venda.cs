using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.src.Model
{
    public class Venda
    {
        public DateTime Data { get; set; }
        public int IdPedido { get; set; }
        public string Itens { get; set; }
        public string Status { get; set; }        
    }
}